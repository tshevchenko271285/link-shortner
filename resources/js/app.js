require('./bootstrap');

window.Vue = require('vue').default;

import shortlinkForm from './components/shortlink-form';
Vue.component('shortlink-form', shortlinkForm);

const app = new Vue({
    el: '#app',
});
