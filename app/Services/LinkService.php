<?php

namespace App\Services;


use App\Http\Resources\LinkResource;
use App\Models\Link;
use App\Services\Contracts\ILinkService;

class LinkService implements ILinkService
{
    /**
     * @param string $shortlink
     * @return LinkResource|null
     */
    public function getLinkByShortLink(string $shortlink): ?LinkResource
    {
        $link = Link::where('shortlink', $shortlink)->first();

        return $link ? LinkResource::make($link) : null;
    }

    /**
     * @param array $data
     * @return LinkResource
     */
    public function makeOrFind(array $data): LinkResource
    {
        $link = Link::where('link', $data['link'])->first();

        if(!$link) {
            $link = Link::create([
                'link' => $data['link'],
                'shortlink' => hash('crc32b', $data['link']),
            ]);
        }

        return LinkResource::make($link);
    }
}
