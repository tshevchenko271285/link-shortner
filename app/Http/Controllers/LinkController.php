<?php

namespace App\Http\Controllers;

use App\Http\Requests\MakeLinkRequest;
use App\Services\Contracts\ILinkService;

class LinkController extends Controller
{

    protected ILinkService $linkService;

    public function __construct(ILinkService $linkService)
    {
        $this->linkService = $linkService;
    }

    public function index($shortlink = null)
    {
        if($shortlink) {
            $link = $this->linkService->getLinkByShortLink($shortlink);
            if($link) {
                return redirect($link->link, 301);
            }
        }

        return view('welcome');
    }

    public function makeOrFind(MakeLinkRequest $request)
    {
        return $this->linkService->makeOrFind($request->all());
    }
}
